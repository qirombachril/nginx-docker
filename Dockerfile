FROM ubuntu:18.04
RUN apt-get update && apt-get install --no-install-recommends -y nginx && rm -rf /var/lib/apt/lists/*
COPY ./hello.txt /var/www/
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
